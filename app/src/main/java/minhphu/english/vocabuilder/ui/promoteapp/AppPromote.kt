package minhphu.english.vocabuilder.ui.promoteapp

import android.content.Context
import minhphu.english.vocabuilder.R

class AppPromote(
        var appName: String,
        var appIcon: Int,
        var appUrl: String,
        var isMoreApp: Boolean = false
) {
    companion object {
        fun getListPromoteApp(context: Context): ArrayList<AppPromote> {
            val listMoreApp = ArrayList<AppPromote>()

            listMoreApp.add(
                    AppPromote(
                            "English Grammar",
                            R.drawable.grammar_icon_256,
                            context.getString(R.string.package_grammar)
                    )
            )

            listMoreApp.add(
                    AppPromote(
                            "English Essay",
                            R.drawable.essay_256,
                            context.getString(R.string.package_essay)
                    )
            )

            listMoreApp.add(
                    AppPromote(
                            "English Error Finding",
                            R.drawable.error_finding_256,
                            context.getString(R.string.package_error_finding)
                    )
            )

            listMoreApp.add(
                    AppPromote(
                            "TOEFL Practice Test",
                            R.drawable.toefl_test_256,
                            context.getString(R.string.package_toefl_test)
                    )
            )

            listMoreApp.add(
                    AppPromote(
                            "IELTS Grammar Test",
                            R.drawable.ielts_test_256,
                            context.getString(R.string.package_ielts_test)
                    )
            )

            listMoreApp.add(
                    AppPromote(
                            "More App",
                            R.drawable.icon_dev,
                            context.getString(R.string.dev_page), true
                    )
            )
            return listMoreApp
        }
    }
}