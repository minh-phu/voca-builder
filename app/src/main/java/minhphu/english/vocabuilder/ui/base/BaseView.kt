package minhphu.english.vocabuilder.ui.base

import android.content.Context
import io.reactivex.disposables.Disposable

interface BaseView {
    fun addDispose(d: Disposable)
    fun getMyContext(): Context?
}