package minhphu.english.vocabuilder

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.google.android.gms.ads.MobileAds
import io.fabric.sdk.android.Fabric
import minhphu.english.vocabuilder.utils.CipherHelper
import minhphu.english.vocabuilder.utils.tts.TextToSpeechManager

class MyApplication : MultiDexApplication() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        Fabric.with(this, Answers())
        MobileAds.initialize(applicationContext, getString(R.string.app_ads_id))
        TextToSpeechManager.getInstance(this)
        CipherHelper(getSecretKey())
    }

    private external fun getSecretKey(): String

    companion object {
        init {
            System.loadLibrary("native-lib")
        }
    }
}
