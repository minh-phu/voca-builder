package minhphu.english.vocabuilder.data.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import minhphu.english.vocabuilder.data.database.entities.Lesson
import minhphu.english.vocabuilder.data.database.repository.LessonRepository

class LessonViewModel(lessonRepository: LessonRepository) : ViewModel() {

    var mListLesson: LiveData<List<Lesson>> = lessonRepository.getListLesson()

}