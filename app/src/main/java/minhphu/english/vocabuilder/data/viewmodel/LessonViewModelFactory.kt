package minhphu.english.vocabuilder.data.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import minhphu.english.vocabuilder.data.database.repository.LessonRepository

class LessonViewModelFactory(
    private val lessonRepository: LessonRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return LessonViewModel(lessonRepository) as T
    }
}