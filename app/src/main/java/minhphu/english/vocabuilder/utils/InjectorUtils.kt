package minhphu.english.vocabuilder.utils

import android.content.Context
import minhphu.english.vocabuilder.data.database.AppDatabase
import minhphu.english.vocabuilder.data.database.daos.LessonDao
import minhphu.english.vocabuilder.data.database.daos.WordDao
import minhphu.english.vocabuilder.data.database.repository.LessonRepository
import minhphu.english.vocabuilder.data.viewmodel.LessonViewModelFactory

object InjectorUtils {
    private fun getLessonRepository(context: Context): LessonRepository {
        return LessonRepository.getInstance(provideLessonDao(context))
    }

    fun provideLessonViewModelFactory(context: Context): LessonViewModelFactory {
        val repository = getLessonRepository(context)
        return LessonViewModelFactory(repository)
    }

    fun provideLessonDao(context: Context): LessonDao {
        return AppDatabase.getInstance(context).lessonDao()
    }

    fun provideWordDao(context: Context): WordDao {
        return AppDatabase.getInstance(context).wordDao()
    }
}
